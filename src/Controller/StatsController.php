<?php

namespace App\Controller;

use App\Entity\SaleData;
use App\Repository\CategoryRepository;
use App\Repository\CommandeRepository;
use App\Repository\PanierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class StatsController extends AbstractController
{
    /**
     * @Route("/stats", name="stats")
     */
    public function index(CommandeRepository $commandeRepository,
    PanierRepository $panierRepository, Request $request): Response
    {

        $dateStart = $request->query->get('dateStart');
        $dateEnd = $request->query->get('dateEnd');


       // dd($commandeRepository->countByDate($dateStart, $dateEnd));
        $nbCommand = count($commandeRepository->findByDate($dateStart, $dateEnd));
        $nbPanier = $panierRepository->count([]);

        $montant = 0;
        $commandes = $commandeRepository->findByDate($dateStart, $dateEnd);
        foreach ($commandes as $commande){
            foreach ($commande->getLignesCommandes() as $ligne){
               $montant += $ligne->getQuantite() * $ligne->getPrixHt();
            }
        }
        return new JsonResponse(
            [
                "montantVenteTotal"=> $montant,
                'nbCommand'=> $nbCommand,
                'nbPanier'=> $nbPanier
            ]
        );
    }

    /**
     * @Route("/graph-1", name="app_stats_graph1")
     */
    public function graph1(CategoryRepository $cr, SerializerInterface $serializer,
                           Request $request): Response
    {
        $dateStart = $request->query->get('dateStart');
        $dateEnd = $request->query->get('dateEnd');

        if($dateStart){
            $dateStart = new \DateTime($dateStart);
        }

        if($dateEnd){
            $dateEnd = new \DateTime($dateEnd);
        }



        $categories = $cr->findAll();

        $retour = [];

        foreach ($categories as $categ){
            $saleData = new SaleData();
            $montant = 0;

            foreach ($categ->getProduits() as $produit){
                foreach ($produit->getLignesCommandes() as $line){
                   $dateCommand = $line->getCommande()->getDateCommande();

                    if(is_null($dateStart) && is_null($dateEnd)){
                        $montant += $line->getQuantite()*$line->getPrixHt();
                    }

                    if(is_null($dateEnd) and !is_null($dateStart)){
                        if($dateCommand>=$dateStart){
                            $montant += $line->getQuantite()*$line->getPrixHt();
                        }
                    }

                    if(is_null($dateStart) and !is_null($dateEnd)){
                        if($dateCommand<=$dateEnd){
                            $montant += $line->getQuantite()*$line->getPrixHt();
                        }
                    }

                    if(!is_null($dateStart) && !is_null($dateEnd)){

                        if($dateCommand<=$dateEnd && $dateCommand>=$dateStart){
                            $montant += $line->getQuantite()*$line->getPrixHt();
                        }
                    }
                }
            }

            $saleData->setName($categ->getLabel());
            $saleData->setValue($montant);

            $retour[] = $saleData;

        }

        return new Response($serializer->serialize(
            $retour,'json'
        ), 200, [
            'Content-Type'=> 'application/json'
        ]);
    }



}
