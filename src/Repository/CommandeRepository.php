<?php
// src/Repository/ProductRepository.php
namespace App\Repository;

use App\Entity\Commande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CommandeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commande::class);
    }

    public function findByDate($dateStart, $dateEnd){


        $query = $this->createQueryBuilder('commande');
        if(!is_null($dateStart)) {
            $query->andWhere('commande.dateCommande >= :dateStart');
            $query->setParameter('dateStart', $dateStart);
        }



        if(!is_null($dateEnd)) {
            $query->andWhere('commande.dateCommande <= :dateEnd');
            $query->setParameter('dateEnd', $dateEnd);
        }

          return  $query->getQuery()
            ->getResult();
    }
}