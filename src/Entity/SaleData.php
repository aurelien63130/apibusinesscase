<?php

namespace App\Entity;

use App\Repository\SaleDataRepository;
use Doctrine\ORM\Mapping as ORM;

class SaleData
{

    private $name;

    private $value;


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }
}
